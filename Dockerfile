FROM golang:alpine as builder
LABEL maintainer="tomasz.szymanski@greenit.com.pl"
WORKDIR /code/
RUN apk add --update curl && \
    rm -rf /var/cache/apk/* && \
    curl https://raw.githubusercontent.com/jorgemoralespou/ose-chained-builds/master/go-scratch/hello_world/main.go -o main.go 
# budujemy aplikacje
# RUN go build -o main .
RUN CGO_ENABLED=0 GOOS=linux go build -v -a -tags netgo -ldflags '-w' -o main .

FROM scratch
COPY --from=builder /code/main /main
EXPOSE 8080/tcp
CMD ["/main"]

